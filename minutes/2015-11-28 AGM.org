* Welcome to the 2015 Annual General Meeting
Introduce myself as treasurer and secretary, "The Keeper of the
Scrolls", as appointed by the Directors.

Introduce the Directors.
* Do we have a quorum?
Ten voting members required, or we must reconvene! James, Chas, Neil,
Andy J, Alaric, Andy Savery are on the list so far. Find four more!

To be a voting member: Offer up a £1 deposit (returnable!) and give
name+address to Neil.


...no, we didn't get a quorum. AGM adjourned.
