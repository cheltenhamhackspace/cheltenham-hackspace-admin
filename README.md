# README #

Welcome to the Cheltenham Hackspace Admin repository.

Herein are all the public formal records of Cheltenham Hackspace Ltd - the articles (which form the "constitution" of the organisation), the minutes of meetings, and our financial records.

Our membership records are private, as they identify individuals, so are kept elsewhere. But anything we can share is in here, for immediate access by our members.