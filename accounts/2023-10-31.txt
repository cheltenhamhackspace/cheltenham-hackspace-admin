expense type:WASTE_DISPOSAL amount:12.41 account:BANK_LLOYDS txnid:f5dd5c063670e348350b493b0d91552ebcc1aea9
member_service_fee member:999999 amount:62.53 service:'Membership' account:BANK_LLOYDS txnid:1c20605a4ec12136db9c15bc217d79ac3a97fc82

# 3 year projected lifespan
depreciation asset:"Black 3D Printer" amount:56.66
depreciation asset:"Misc" amount:13.26

# 5 year depreciations
depreciation asset:"Amenities" amount:21
depreciation asset:"Column drill" amount:15
depreciation asset:"Electric kiln" amount:20
depreciation asset:"Furniture" amount:168.51
depreciation asset:"Hand tools" amount:37.01
depreciation asset:"Laser cutter" amount:13.33
depreciation asset:"PCB etching" amount:28 # Last one!
depreciation asset:"Power tools" amount:20.39
depreciation asset:"Soldering iron" amount:40
depreciation asset:"Test gear" amount:44
depreciation asset:"Textile tools" amount:29.80

# Not really depreciating as we're not using them
# depreciation asset:"Arcade cabinet"
# depreciation asset:"Traffic lights"

# Not really depreciating as we turn over consumables (and they're mostly antiques that don't go out of date...)
# depreciation asset:"Consumables"
