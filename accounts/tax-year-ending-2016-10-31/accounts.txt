


                            Registered number 9269647

                          CHELTENHAM HACKSPACE LIMITED

                              Abbreviated Accounts

                             Year ending 2016-10-31

                                REVISED ACCOUNTS
                      COMPANIES HOUSE REF: ACT/09269647/B E
                            REVISION DATE: 2017-08-25

These accounts replace the previous accounts filed, and are now the
statutory accounts.

They have been prepared as at the date of the original accounts and
not as at the date of revision and accordingly do not deal with events
between those dates.

The previous accounts were generated via the gov.uk "file with
companies house and HMRC in one go" tool, based on numbers copied from
the internal management accounting software. However, a software error
led to depreciation being incorrectly accounted for, and the
subsequent internal review also led to the identification of a further
issue: £41.64 of stock (some polo shirts) had been misclassified as a
fixed asset.

These new accounts have been generated directly from our internal
records, without any manual copying of figures into the gov.uk Web
form; as the results balance, we can now be confident that these truly
reflect our financial position at the end of the accounting period.


            CHELTENHAM HACKSPACE LIMITED - REGISTERED NUMBER 9269647


                   Abbreviated Balance Sheet as at 2016-10-31

                                                Notes 2016-10-31 2015-11-01
                                                              £         £

FIXED ASSETS
                         Intangible assets                     -          -
                           Tangible assets        [3]    1936.91    1764.80
                               Investments                     -          -
                                                           -----     ------
                                                         1936.91    1764.80
                                                           -----     ------

CURRENT ASSETS
                                    Stocks                     -          -
                                   Debtors                     -          -
                               Investments                     -          -
                                     Stock                 41.64          -
                  Cash at bank and in hand               1759.93     982.32
                                                           -----     ------
                                                         1801.57     982.32
                                                           -----     ------

            Prepayments and accrued income                     -          -
            Creditors: due within one year              (102.80)          -
          Net current assets (liabilities)               3738.48    2747.12
     Total assets less current liabilities               3635.68    2747.12
   Creditors: due after more than one year                     -          -
                Provisions for liabilities                     -          -
              Accruals and deferred income                     -          -
            Total net assets (liabilities)               3635.68    2747.12

RESERVES
                       Revaluation reserve                     -          -
                            Other reserves                     -          -
            Income and expenditure account               3635.68    2747.12

                            Member's Funds               3635.68    2747.12

            CHELTENHAM HACKSPACE LIMITED - REGISTERED NUMBER 9269647

 * For the year ending 2016-10-31 the company was entitled to exemption
   under section 477 of the Companies Act relating to small companies.

 * The members have not required the company to obtain an audit in
   accordance with section 476 of the Companies Act 2006.

 * The directors acknowledge their responsiblities for complying with
   the requirements of the Act with respect to account periods and
   the preparatino of accounts.

 * These accounts have been prepared in accordance with the provisions
   applicable to companies subject to the small companies regime.

Approved by the Board on 29th July 2017
And signed on their behalf by:

            CHELTENHAM HACKSPACE LIMITED - REGISTERED NUMBER 9269647

                        Notes to the Abbreviated Accounts

 [1] Accounting policies

The accounts have been prepared under the historical cost convention
and in accordance with the Financial Reporting Standard for Smaller
Entities effective April 2008.

 [2] Company limited by guarantee

Company is limited by guarantee and consequently does not have share capital.

 [3] Tangible fixed assets

                                                                         £
COST
                             At 2015-11-01                          1764.80
                                 Additions                           262.26
                                 Disposals                                -
                              Revaluations                                -
                                 Transfers                                -
                             At 2016-10-31                          2027.06

DEPRECIATION
                             At 2015-11-01                                -
                       Charge for the year                            90.15
                             At 2016-10-31                            90.15

NET BOOK VALUES
                             At 2015-11-01                          1764.80
                             At 2016-10-31                          1936.91
