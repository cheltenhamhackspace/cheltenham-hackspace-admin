Here begins the sorry tale of the recovery of our financial records from the paper Barclays statements.

We have electronic bank statements up to 2022-07-20 already imported into the accounts system.

But then they killed our account, and sent us paper statements :-(

These are in the format of monthly sections:

1) Header page saying (eg) "Your balances on 24 February 2020"
2) Several numbered pages of transactions:
   - Date
   - Description
   - Money out £
   - Money in £
   - Balance £ (AFTER that transaction happened)
3) Several numbered pages of FAQs and other irrelevant static information

What we need is to convert that into, perhaps monthly summaries that total up:

 - Money received from members
 - Honesty box income
 - Electricity income
 - Any refunds/repayments
 - Asset/stock sales
 - Any other income

 - Rent
 - Electricity bill
 - Consumables Purchases
 - Services payments
 - Asset purchases
 - Any other expenses

The start of a month happens somewhere mid-page.

As most of the transactions are money received from members, I propose I enter data in this form:

 - For a monthly section, the "Your balances on... " date as a section identifier
   - For each subpage of useful information there, identified by a page number and an optional "a/b" if it's a page that gets split by a month boundary
     - The first date mentioned on the page
     - The last date mentioned on the page
     - The final balance on the page
     - Totals for any txns OTHER THAN money from members

Eg, pages all in the same month will be recorded as "Page 2", but if it covers a month boundary we'll record separate "Page 2a" up to the 31st and "Page 2b" from the 1st onwards.

The software can then subtract the final balance from the previous page's final balance to get a total delta, then subtract out all the other transactions to get a total for money from members. Each page is assigned to a specific month, so it can then spit out a per-month summary of all the pages in that month with totals, and put that into a accounts file dated at the END of the month.

Prototype implementation is in eeeeaarrrgghhh.scm.

1) Install Chicken Scheme

2) "csi -script eeeeaarrrgghhh.scm"

3) Look at the .txt files that get generated.
