(import srfi-1)
(import srfi-69)
(import (chicken format))
(import (chicken sort))

(define *statements* '())

(define-record-type page
  (make-page number month first-day last-day last-balance txns)
  page?
  (number page-number)
  (month page-month)
  (first-day page-first-day)
  (last-day page-last-day)
  (last-balance page-last-balance)
  (txns page-transactions))

(define-record-type transaction
  (make-transaction* type amount)
  transaction?
  (type transaction-type)
  (amount transaction-amount))

(define (make-transaction type amount)
  (assert (symbol? type))
  (assert (number? amount))
  (assert (exact? amount))
  (make-transaction* type amount))

(define (statement yyyy-mm-dd pages)
  (assert (string? yyyy-mm-dd))
  (assert (list? pages))

  (printf "Loading ~A\n" yyyy-mm-dd)
  
  (set! *statements*
        (cons
         (cons yyyy-mm-dd pages)
         *statements*)))

(define (page number month first-dd last-dd last-balance txns)
  (assert (string? number))
  (assert (string? month))
  ;; FIXME: Check format
  (assert (number? first-dd))
  (assert (<= 1 first-dd 31))
  (assert (number? last-dd))
  (assert (<= 1 last-dd 31))
  (assert (number? last-balance))
  (assert (exact? last-balance))

  (make-page number month first-dd last-dd last-balance txns))

(define (virgin amount)
  (assert (= amount 48))
  (make-transaction 'virgin (- amount)))

(define (rent amount)
  (assert (or
           (= amount 938)
           (= amount 966)))
  (make-transaction 'rent (- amount)))

(define (electricity amount)
  (assert (= amount #e166.74))
  (make-transaction 'buy-electricity (- amount)))

(define (export-and-general amount)
  (make-transaction 'insurance (- amount)))

(define (bank-fee amount)
  (assert (= amount #e8.50))
  (make-transaction 'bank-commission (- amount)))

(define (sell-electricity amount)
  (make-transaction 'sell-electricity amount))

(define (asset-purchase thing cost)
  (make-transaction 'asset-purchase (- cost)))

;; The data, in order

(define initial-balance #e10578.29)

(statement "2021-11-24"
           (list
            (page "1b" "2021-11" 1 1 #e9652.29
                  (list (virgin #e48)
                        (rent #e938)))
            (page "2" "2021-11" 1 12 #e10135.03
                  (list (bank-fee #e8.50)))
            (page "3" "2021-11" 15 24 #e10418.07
                  (list))))

(statement "2021-12-24"
           (list
            (page "1a" "2021-11" 25 30 #e10367.37
                  (list
                   (electricity #e166.74)))
            (page "1b" "2021-12" 1 1 #e9441.37
                  (list
                   (virgin #e48)
                   (rent #e938)))
            (page "2" "2021-12" 1 16 #e9957.03
                  (list
                   (bank-fee #e8.50)))
            (page "3" "2021-12" 16 24 #e10171.51
                  (list))))

(statement "2022-01-24"
           (list
            (page "1a" "2021-12" 29 31 #e10159.37
                  (list
                   (electricity #e166.74)))
            (page "1b" "2022-01" 4 4 #e9243.37
                  (list
                   (virgin #e48)
                   (rent #e938)))
            (page "2" "2022-01" 4 19 #e9780.15
                  (list
                   (bank-fee #e8.50)))
            (page "3" "2022-01" 20 24 #e9878.47
                  (list))))

(statement "2022-02-24"
           (list
            (page "1a" "2022-01" 25 31 #e10085.49
                  (list (electricity #e166.74)
                        (sell-electricity #e219.16)))
            (page "1b" "2022-02" 1 1 #e9139.49
                  (list (virgin #e48)
                        (rent #e938)))
            (page "2" "2022-02" 1 11 #e9603.31
                  (list (bank-fee #e8.50)))
            (page "3" "2022-02" 14 24 #e9837.42
                  (list (asset-purchase "bandsaw" #e58.57)))))

(statement "2022-03-24"
           (list
            (page "1a" "2022-02" 25 28 #e9844.24
                  (list (electricity #e166.74)))
            (page "1b" "2022-03" 1 1 #e8928.24
                  (list (virgin #e48)
                        (rent #e938)))
            (page "2" "2022-02" 1 15 #e9487.74
                  (list
                   (bank-fee #e8.50)))
            (page "3" "2022-02" 16 24 #e9692.58
                  (list))))

(statement "2022-04-22"
           (list
            (page "1a" "2022-03" 25 31 #e9670.80
                  (list
                   (electricity #e166.74)))
            (page "1b" "2022-04" 1 1 #e8734.80
                  (list
                   (virgin #e48)
                   (rent #e938)))
            (page "2" "2022-04" 1 14 #e9209.76
                  (list (bank-fee #e8.50)))
            (page "3" "2022-04" 19 22 #e9366.76
                  (list))))

(statement "2022-05-24"
           (list
            (page "1a" "2022-04" 25 29 #e9334.98
                  (list
                   (electricity #e166.74)))
            (page "1b" "2022-05" 3 3 #e8380.98
                  (list (virgin #e48)
                        (rent #e966)))
            (page "2" "2022-05" 3 13 #e8873.72
                  (list (bank-fee #e8.50)))
            (page "3" "2022-05" 16 24 #e9127.48
                  (list))))
(statement "2022-06-24"
           (list
            (page "1a" "2022-05" 25 31 #e9105.70
                  (list (electricity #e166.74)))
            (page "1b" "2022-06" 1 1 #e8141.70
                  (list (virgin #e48)
                        (rent #e966)))
            (page "2" "2022-06" 1 20 #e8674.08
                  (list (bank-fee #e8.50)))
            (page "3" "2022-06" 20 24 #e8849.64
                  (list))))
(statement "2022-07-22"
           (list
            (page "1a" "2022-06" 25 30 #e8789.30
                  (list (electricity #e166.74)))
            (page "1b" "2022-07" 1 1 #e7835.30
                  (list (virgin #e48)
                        (rent #e966)))
            (page "2" "2022-07" 1 18 #e8347.44
                  (list (bank-fee #e8.50)))
            (page "3" "2022-07" 18 22 #e8535.00
                  (list))))
(statement "2022-08-24"
           (list
            (page "1a" "2022-07" 23 29 #e8503.22
                  (list (electricity #e166.74)))
            (page "1b" "2022-08" 1 1 #e7549.22
                  (list (virgin #e48)
                        (rent #e966)))
            (page "2" "2022-08" 1 11 #e7693.56
                  (list (bank-fee #e8.50)
                        (export-and-general #e299.60)))
            (page "3" "2022-08" 12 24 #e8532.84
                  (list))))
(statement "2022-09-23"
           (list
            (page "1a" "2022-08" 25 31 #e8511.06
                  (list (electricity #e166.74)))
            (page "1b" "2022-09" 1 1 #e7557.06
                  (list (virgin #e48)
                        (rent #e966)))
            (page "2" "2022-09" 1 20 #e8097.76
                  (list (bank-fee #e8.50)))
            (page "3" "2022-09" 20 23 #e8274.04
                  (list))))
(statement "2022-10-24"
           (list
            (page "1a" "2022-09" 26 30 #e8242.26
                  (list (electricity #e166.74)))
            (page "1b" "2022-10" 3 3 #e7288.26
                  (list (virgin #e48)
                        (rent #e966)))
            (page "2" "2022-10" 3 14 #e7838.24
                  (list (bank-fee #e8.50)))
            (page "3" "2022-10" 17 24 #e8058.44
                  (list))))
(statement "2022-11-24"
           (list
            (page "1a" "2022-10" 25 31 #e8026.66
                  (list (electricity #e166.74)))
            (page "1b" "2022-11" 1 1 #e7072.66
                  (list (virgin #e48)
                        (rent #e966)))
            (page "2" "2022-11" 1 14 #e7589.26
                  (list (bank-fee #e8.50)))
            (page "3" "2022-11" 14 24 #e8407.77
                  (list (sell-electricity #e472.99)))))
(statement "2022-12-23"
           (list
            (page "1a" "2022-11" 25 30 #e8327.79
                  (list (electricity #e166.74)))
            (page "1b" "2022-12" 1 1 #e7383.79
                  (list (virgin #e48)
                        (rent #e966)))
            (page "2" "2022-12" 1 14 #e7996.91
                  (list (bank-fee #e8.50)))
            (page "3" "2022-12" 14 23 #e8246.77
                  (list))))
(statement "2023-01-24"
           (list
            (page "1a" "2022-12" 24 30 #e8186.07
                  (list (electricity #e166.74)))
            (page "1b" "2023-01" 3 3 #e7252.07
                  (list (virgin #e48)
                        (rent #e966)))
            (page "2" "2023-01" 3 16 #e7855.37
                  (list (bank-fee #e8.50)))
            (page "3" "2023-01" 1 24 #e8151.02
                  (list))))

;; Output generation

(define (sum-txns txns)
  (fold
   (lambda (txn sum)
     (+ sum (transaction-amount txn)))
   0
   txns))

(define *months* (make-hash-table))

(define (file-txn-in-month statement-id page txn)
  (if (hash-table-exists? *months* (page-month page))
      (hash-table-set! *months* (page-month page)
                       (cons (vector txn statement-id (page-number page))
                             (hash-table-ref *months* (page-month page))))
      (hash-table-set! *months* (page-month page)
                       (list (vector txn statement-id (page-number page))))))

(define (fmt-money amount)
  ;; FIXME: Nice formatting please
  (sprintf "~A" (exact->inexact amount)))

;; Summarise statements and accumulate months

(printf "STATEMENTS:\n\n")

(let ((previous-page-balance initial-balance))
 (for-each
  (lambda (statement)
    (let ((statement-id (car statement))
          (pages (cdr statement)))
      (printf "~A: ~A pages\n" statement-id (length pages))
      (for-each
       (lambda (page)
         (let ((delta (- (page-last-balance page) previous-page-balance)))
           (printf "Page ~A: ~A-~A -> ~A-~A\n"
                   (page-number page)
                   (page-month page)
                   (page-first-day page)
                   (page-month page)
                   (page-last-day page))
           (printf " Balance: ~A -> ~A (~A)\n" (fmt-money previous-page-balance)
                   (fmt-money (page-last-balance page))
                   (fmt-money delta))

           ;; Add up txns and generate income txn
           (let ((transactions
                  (cons
                   (let ((txn-total (sum-txns (page-transactions page))))
                     (make-transaction 'members-fees (- delta txn-total)))
                   (page-transactions page))))
             (for-each
              (lambda (txn)
                (file-txn-in-month statement-id page txn)
                (printf " ~A: ~A\n"
                        (transaction-type txn)
                        (fmt-money (transaction-amount txn))))
              transactions))
           (set! previous-page-balance (page-last-balance page))))
       pages)))
  (reverse *statements*)))

;; Summarise months

(printf "\nMONTHS:\n\n")

(let* ((months (hash-table-keys *months*))
       (sorted-months (sort months
                            string<?)))
  (for-each
   (lambda (month)
     (let ((month-data (hash-table-ref *months* month))
           (month-summary (make-hash-table))
           (month-txn-file (open-output-file (sprintf "~A-01.txt" month))))
       (fprintf month-txn-file "# Autogenerated by eeeeaarrrgghhh.scm, from data entered by hand from Barclays paper statements\n")
       (fprintf month-txn-file "# The exact day on which these transactions happen is recorded in the paper forms, but not entered\n")
       (fprintf month-txn-file "# here. Also, all member service fees are rolled up into a single transaction per entire page of bank\n")
       (fprintf month-txn-file "# statement (unless that page crosses a month boundary, in which case we get a transaction per page per month)\n")
       (fprintf month-txn-file "\n")
       (printf "MONTH ~A:\n" month)
       (for-each
        (lambda (txn-data)
          (let ((txn (vector-ref txn-data 0))
                (statement-id (vector-ref txn-data 1))
                (page-number (vector-ref txn-data 2)))
            (hash-table-set! month-summary (transaction-type txn)
                             (+ (transaction-amount txn)
                                (hash-table-ref/default
                                 month-summary
                                 (transaction-type txn)
                                 #e0)))
            (printf " ~A: ~A (statement ~A page ~A)\n"
                    (transaction-type txn)
                    (fmt-money (transaction-amount txn))
                    statement-id page-number)
            (let ((txnid (sprintf "barclays_~A_page_~A" statement-id page-number)))
             (case (transaction-type txn)
               ((members-fees)
                (fprintf month-txn-file "member_service_fee member:999999 amount:~A service:'Membership' account:BANK txnid:~A\n"
                         (fmt-money (transaction-amount txn))
                         txnid))
               ((virgin)
                (fprintf month-txn-file "expense type:VIRGIN_MEDIA amount:~A account:BANK txnid:~A\n"
                         (fmt-money (- (transaction-amount txn)))
                         txnid))
               ((buy-electricity)
                (fprintf month-txn-file "expense type:ELECTRICITY amount:~A account:BANK txnid:~A\n"
                         (fmt-money (- (transaction-amount txn)))
                         txnid))
               ((bank-commission)
                (fprintf month-txn-file "expense type:BANK_CHARGES amount:~A account:BANK txnid:~A\n"
                         (fmt-money (- (transaction-amount txn)))
                         txnid))
               ((rent)
                (fprintf month-txn-file "expense type:RENT amount:~A account:BANK txnid:~A\n"
                         (fmt-money (- (transaction-amount txn)))
                         txnid))
               ((insurance)
                (fprintf month-txn-file "expense type:INSURANCE amount:~A account:BANK txnid:~A\n"
                         (fmt-money (- (transaction-amount txn)))
                         txnid))
               ((asset-purchase)
                (fprintf month-txn-file "asset_purchase name:FIXME serial:FIXME description:FIXME depreciation:FIXME amount:~A account:BANK txnid:~A\n"
                         (fmt-money (- (transaction-amount txn)))
                         txnid))
               ((sell-electricity)
                (fprintf month-txn-file "external_income from:'Electricity' service:'Electricity' amount:~A account:BANK txnid:~A\n"
                         (fmt-money (transaction-amount txn))
                         txnid))
               (else (error "Unknown transaction type" (transaction-type txn)))))))
        month-data)
       (printf " SUMMARY:\n")
       (hash-table-for-each month-summary
                            (lambda (key value)
                              (printf "  ~A: ~A\n" key (fmt-money value))))
       (close-output-port month-txn-file)))
   sorted-months))
