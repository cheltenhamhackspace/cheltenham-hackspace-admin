   Income and Expenditure account for the period from 2018-11-01 to 2019-10-31

                                                      2019-10-31 2018-11-01     Change
                                                Notes         £         £         £

                            Trading Income               8628.42    6401.62    2226.80
                    Donations from members              22205.71   17202.71    5003.00
                Donations from non-members              15841.47    8026.22    7815.25
                                                        --------                      
                                  Turnover              46675.60   31630.55   15045.05

                             Cost of sales                195.80     195.80          -
                             Gross surplus              46479.80   31434.75   15045.05
                                  Expenses              39324.94   26415.25   12909.69
                              Depreciation               1171.47     688.50     482.97
                                                        ========                      
                 Operating surplus/defecit               5983.39    4331.00    1652.39


Breakdown of Trading Income
                     Science Festival 2018               1200.00    1200.00          -
                            Refund ACC fee                  6.50       6.50          -
                               Electricity               1831.43     735.30    1096.13
            Rent refund due to overpayment                369.00     369.00          -
                     Science Festival 2017               1000.00    1000.00          -
               Access to space for 1 month               1458.00    1161.00     297.00
                         Honesty box sales               2235.23    1429.13     806.10
                            Loyalty Reward                 12.56       7.69       4.87
                             Making things                322.70     300.00      22.70
                               Asset sales                 57.00      57.00          -
                               Stock sales                   136        136          -
                                                        --------   --------   --------
                                                         8628.42    6401.62    2226.80

Breakdown of Expenses
                                  FITTINGS                786.11     786.11          -
                           GoCardless test                  0.01       0.01          -
                                    Snacks                111.94      33.30      78.64
                               ELECTRICITY               3458.90    2045.96    1412.94
                               CONSUMABLES                478.77     478.77          -
                              BANK_CHARGES                287.27     190.90      96.37
                                 INSURANCE                944.47     685.73     258.74
                                      RENT              30561.00   20109.00   10452.00
                              VIRGIN_MEDIA               1884.00    1308.00     576.00
                                 LASERTUBE                   138        138          -
                                Petty cash                577.77     542.77      35.00
                                      ADSL                 10.00      10.00          -
                  ANNUAL_RETURN_FILING_FEE                    13         13          -
                     Returned test payment                 10.00      10.00          -
              Science Festival Consumables                 63.70      63.70          -
                                                        --------   --------   --------
                                                        39324.94   26415.25   12909.69
