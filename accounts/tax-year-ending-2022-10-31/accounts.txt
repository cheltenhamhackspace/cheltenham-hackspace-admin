


                            Registered number 9269647

                          CHELTENHAM HACKSPACE LIMITED

                              Abbreviated Accounts

                             Year ending 2022-10-31

            CHELTENHAM HACKSPACE LIMITED - REGISTERED NUMBER 9269647


                   Abbreviated Balance Sheet as at 2022-10-31

                                                Notes 2022-10-31 2021-11-01           
                                                              £         £           

FIXED ASSETS
                         Intangible assets                     -          -           
                           Tangible assets        [3]    2393.69    2706.26           
                               Investments                     -          -           
                                                           -----     ------           
                                                         2393.69    2706.26           
                                                           -----     ------           

CURRENT ASSETS
                                    Stocks                     -          -           
                                   Debtors                     -          -           
                               Investments                     -          -           
                                     Stock                     -          -           
                  Cash at bank and in hand               8603.70   11155.33           
                                                           -----     ------           
                                                         8603.70   11155.33           
                                                           -----     ------           

            Prepayments and accrued income                     -          -           
            Creditors: due within one year                     -          -           
          Net current assets (liabilities)               8603.70   11155.33           
     Total assets less current liabilities              10997.39   13861.59           
   Creditors: due after more than one year                     -          -           
                Provisions for liabilities                     -          -           
              Accruals and deferred income                     -          -           
            Total net assets (liabilities)              10997.39   13861.59           

RESERVES
                       Revaluation reserve                     -          -           
                            Other reserves                     -          -           
            Income and expenditure account              10997.39   13861.59           

                            Member's Funds              10997.39   13861.59           

            CHELTENHAM HACKSPACE LIMITED - REGISTERED NUMBER 9269647

 * For the year ending 2022-10-31 the company was entitled to exemption
   under section 477 of the Companies Act relating to small companies.

 * The members have not required the company to obtain an audit in
   accordance with section 476 of the Companies Act 2006.

 * The directors acknowledge their responsiblities for complying with
   the requirements of the Act with respect to account periods and
   the preparatino of accounts.

 * These accounts have been prepared in accordance with the provisions
   applicable to companies subject to the small companies regime.

Approved by the Board on _________
And signed on their behalf by:

            CHELTENHAM HACKSPACE LIMITED - REGISTERED NUMBER 9269647

                        Notes to the Abbreviated Accounts

 [1] Accounting policies

The accounts have been prepared under the historical cost convention
and in accordance with the Financial Reporting Standard for Smaller
Entities effective April 2008.

 [2] Company limited by guarantee

Company is limited by guarantee and consequently does not have share capital.

 [3] Tangible fixed assets

                                                                         £           
COST
                             At 2021-11-01                          3877.73           
                                 Additions                            58.57           
                                 Disposals                                -           
                              Revaluations                                -           
                                 Transfers                                -           
                             At 2022-10-31                          3936.30           

DEPRECIATION
                             At 2021-11-01                          1171.47           
                       Charge for the year                           371.14           
                             At 2022-10-31                          1542.61           

NET BOOK VALUES
                             At 2021-11-01                          2706.26           
                             At 2022-10-31                          2393.69           
