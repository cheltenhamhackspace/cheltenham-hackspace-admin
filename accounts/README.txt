=== INTRODUCTION ===

This is the Cheltenham Hackspace Emergency Holographic
Treasurer. Please state the nature of the treasure emergency.

Seriously, this directory contains the financial records of Cheltenham
Hackspace, and software tools to automate processing them. This is so
that the treasurer doesn't have to remember what to do all the time
(the software will do the right thing, as long as somebody remembers
to run it), and so the treasurer can be easily replaced if he is
damaged beyond repair while hacking.

The important things in here are:

1) [date].txt files, listing financial things that happened on that
date.

2) bank-statement-to-[date].tsv files, containing bank
statements. These are just kept for historical record - the
information in them will have been folded into the [date].txt files,
as documented below.

3) finance.py, the Python script that does cool stuff with [date].txt
and bank-statement-to-[date].tsv files.

4) README.txt, this file.

5) TODO.org, a text file listing things that should be done, and when.

=== RECORDING THINGS THAT HAPPEN ===

The [date].txt files record financial transactions. The assets and
liabilities of the Hackspace are divided into "accounts".

The accounts we use are:

CASH: Cash owned by the hackspace. Either in the petty cash box in the
hackspace, or in the honesty box, or held by the Treasurer.

Note that in the past (before 2016-11-3) the petty cash box and the
honesty box weren't updated slavishly in these accounts - people
shoved money in it, then took some out to buy biscuits or whatever,
without bothering to tell the poor Treasurer. Instead, whenever the
wodge in there looked like more than £50, the humble Treasurer took
some of it out and paid it into the bank, then recorded it as an
anonymous donation direct to the bank. However, at the ends of
financial reporting periods, we did a stock check, which included
measuring the balance of the cash in the hackspace and donating
to/expensing from the CASH account to make the balance correct.

The amount of petty cash sloshing about kept growing, however, so
after 2016-11-03 we mandated tracking money in/out of the cashbox on a
pad (while keeping a smaller "honesty bowl" for people paying for
snacks etc), which is then recorded as txns against CASH.

CASH/3DP: When we were building the 3D printer, we kept some cash
earmarked for it. So a separate cash account was created for this
purpose.

BANK: Our Barclays Business Bank account. Paying members of the Space
have standing orders to pay their dues into this account, in exchange
for access to the Space. Largely, transactions against the bank are
created automatically by importing a bank statement (as described
below), and then hand-editing the results if needs be.

BANK/3DP: Some of the bank balanced is earmarked for the 3D
printer. This was a bad idea, and just makes the bank accounts a bit
more complex, so it's been merged back in now.

OWED: Money owed to the hackspace. This is mainly used as a transfer
account, when somebody pays a bill for us; we have them donate to
"OWED" and then the expense is paid from "OWED". In effect, they had
some money for us, which was "owed" because they hadn't given it to us
yet; but then rather than giving it to us, they paid an expense for us
from that money. Does that make sense? Accounting can be weird
sometimes.

=== Regular Payments ===

Here's some common payments that need entering, so I can copy and paste them:

Monthly:

expense type:RENT amount:200 account:BANK_LLOYDS
expense type:VIRGIN_MEDIA amount:10 account:BANK_LLOYDS
expense type:WASTE_DISPOSAL amount:12.41 account:BANK_LLOYDS

Annual:

expense type:INSURANCE amount:413.37 account:BANK_LLOYDS # Start of August
expense type:DATA_PROTECTION amount:40 account:BANK_LLOYDS # Start of March

Other:

# We get paid for electricity
external_income from:'Electricity' service:'Electricity' amount:285.77 account:BANK_LLOYDS txnid:780045c31714e5e7aa3943aa891349ead41c628d

# Stocking up at booker
expense type:"Petty cash" amount:21.20 account:BANK_LLOYDS

# Income from honesty boxes
external_income from:"Petty cash" service:"Honesty box sales" amount:21.95 account:BANK_LLOYDS

=== GETTING ACCOUNTING REPORTS ===

To show the balance sheet at the end of a given date (eg, transactions
on that day will be included):

./finance.py [YYYY-MM-DD]

or:

./financy.py NOW

To show a profit and loss report for a time period:

./finance.py [YYYY-MM-DD|NOW] [YYYY-MM-DD|NOW]

The first date is the start, and the second is the end. The range is
inclusive (transactions on both those dates will be reported).

=== RECONCILING WITH THE BANK ===

Alaric has a Python script that converts the CSVs one can download
from Barclays into a "TSV" file. In doing so it maps from member's
references in the bank statement (which contain personally identifying
information) into member numbers, which can be part of the public
record.

Generated TSV files go into git, in this directory, dated.

They can then be imported into the accounts like so:

./finance.py --import-from-bank BANK < [file].tsv

...where "BANK" is the name of the bank account (useful if we have more
than one bank account). This will add transactions to [date].txt
files, and create new ones, to match transactions that are found in
the TSV but not already in the [date].txt files. Therefore, it is safe
to run this command several times in a row from the same TSV -
subsequent runs will do nothing.

You can test it, like so:

./finance.py --test-import-from-bank BANK < [file].tsv

Rather than appending transactions to [date].txt files, this version
of the command will instead write them to [date].txt.tmp files.

=== END OF YEAR ACCOUNTS ===

These legally need to be files with Companies House and sent in with
the tax return, and shown to the members, and so on.

Generate them for a date range like so:

./finance.py --abbreviated-accounts [YYYY-MM-DD|NOW] [YYYY-MM-DD|NOW]

Remember the dates are inclusive - put in the first and last dates of
the financial year.

=== CORPORATION TAX ===

Tax return dates are in TODO.org.

Our UTR (Unique Taxpayer Reference) number is 2372517331. This is needed when communicating with HMRC.

=== DATA PROTECTION ===

We're registered with the ICO because we store CCTV. It costs £40 a year.

Application number: A8637996
