   Income and Expenditure account for the period from 2017-11-01 to 2018-10-31

                                                      2018-10-31 2017-11-01     Change
                                                Notes         £         £         £

                            Trading Income               6401.62    2629.30    3772.32
                    Donations from members              17202.71   12376.71    4826.00
                Donations from non-members               8026.22    3515.66    4510.56
                                                        --------                      
                                  Turnover              31630.55   18521.67   13108.88

                             Cost of sales                195.80     138.80      57.00
                             Gross surplus              31434.75   18382.87   13051.88
                                  Expenses              26415.25   13843.46   12571.79
                              Depreciation                688.50     360.30     328.20
                                                        ========                      
                 Operating surplus/defecit               4331.00    4179.11     151.89


Breakdown of Trading Income
                     Science Festival 2018               1200.00          -    1200.00
                            Refund ACC fee                  6.50       6.50          -
                               Electricity                735.30          -     735.30
            Rent refund due to overpayment                369.00          -     369.00
                     Science Festival 2017               1000.00    1000.00          -
               Access to space for 1 month               1161.00     827.00     334.00
                         Honesty box sales               1429.13     656.14     772.99
                            Loyalty Reward                  7.69       3.66       4.03
                             Making things                300.00          -     300.00
                               Asset sales                 57.00          -      57.00
                               Stock sales                   136        136          -
                                                        --------   --------   --------
                                                         6401.62    2629.30    3772.32

Breakdown of Expenses
                                  FITTINGS                786.11     544.74     241.37
                           GoCardless test                  0.01       0.01          -
                                    Snacks                 33.30      33.30          -
                               ELECTRICITY               2045.96     370.00    1675.96
                               CONSUMABLES                478.77          -     478.77
                              BANK_CHARGES                190.90     111.40      79.50
                                 INSURANCE                685.73     413.37     272.36
                                      RENT              20109.00   11275.00    8834.00
                              VIRGIN_MEDIA               1308.00     732.00     576.00
                                 LASERTUBE                   138          -        138
                                Petty cash                542.77     266.94     275.83
                                      ADSL                 10.00      10.00          -
                  ANNUAL_RETURN_FILING_FEE                    13         13          -
                     Returned test payment                 10.00      10.00          -
              Science Festival Consumables                 63.70      63.70          -
                                                        --------   --------   --------
                                                        26415.25   13843.46   12571.79
