#!/bin/sh

if [ -z "$1" ]
then	
	echo "USAGE: $0 YYYY"
	echo "...generates ./tax-year-ending-YYYY-10-31/*"
	exit 1
fi

YYY1=$(( $1 - 1 ))
YYYY="$1"
START="$YYY1-11-01"
END="$YYYY-10-31"
ROOT="tax-year-ending-$END"

echo "Generating from $START to $END into $ROOT..."

mkdir -p $ROOT

./finance.py --abbreviated-accounts $START $END > $ROOT/accounts.txt
./finance.py $START > $ROOT/starting-balance.txt
./finance.py $END > $ROOT/ending-balance.txt
./finance.py $START $END > $ROOT/profit-and-loss.txt
