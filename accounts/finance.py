#!/usr/bin/env python
# coding=utf8

import os
import sys
import re
import datetime
import decimal

# Turn off the "default account" feature until we actually
# do mainly use the bank.
DEFAULT_ACCOUNT=None
DEFAULT_MEMBER_ACCESS_FEE=decimal.Decimal("1.00")
DEBUG=False

def print_centered(string):
    l = len(string)
    pad = 40-l/2
    print ("%s%s" % (" "*int(pad),string))

def print_newpage():
    print ("")

def print_cols(title,notes,old,new,delta=""):
    print ("  %40s %10s %10s %10s %10s" % (title,notes,new,old,delta))

def print_values(title,notes,old,new,delta=0):
    if old == 0:
        old = "-"
    elif old < 0:
        old = "(%s)" % (-old)

    if new == 0:
        new = "-"
    elif new < 0:
        new = "(%s)" % (-new)

    if delta == 0:
        delta = "-"
    elif delta < 0:
        delta = "(%s)" % (-delta)

    print_cols(title,notes,old,new,delta)

def print_value(title,val):
    if val == 0:
        val = "-"
    elif val < 0:
        val = "(%s)" % (-old)

    print_cols(title,"",val,"")

def print_subtotal(old,new):
    print_cols("","","------","-----")
    print_values("","",old,new)
    print_cols("","","------","-----")

class Asset:
    def __init__(self, name, serial, descr, date_got, value_when_got, value_loss_per_year, isStock):
        self.name = name
        self.serial = serial
        self.descr = descr
        self.date_got = date_got
        self.value_when_got = value_when_got
        self.value_loss_per_year = value_loss_per_year
        self.value = value_when_got
        self.isStock = isStock
        if DEBUG:
            print ("Asset %s: value=%s" % (name, value_when_got))

    def appreciate(self, added_value, added_depreciation, added_descr):
        self.value += added_value
        self.value_when_got += added_value
        self.value_loss_per_year += added_depreciation
        self.descr = "%s + %s" % (self.descr, added_descr)
        if DEBUG:
            print ("Asset %s: value+=%s, now=%s" % (self.name, added_value, self.value))

    def depreciate(self, amount, descr=None):
        self.value -= amount
        if descr:
            self.descr = "%s - %s" % (self.descr, descr)
        if DEBUG:
            print ("Asset %s: value-=%s, now=%s" % (self.name, amount, self.value))

    def cost(self):
        return self.value_when_got

    def depreciation(self):
        return self.value_when_got - self.value

    def compute_depreciation(self):
        if self.value_loss_per_year > self.value:
            return self.value
        else:
            return self.value_loss_per_year

class Accounts:
    def __init__(self, maxdate):
        self.maxdate = maxdate
        self.donation_total = {} # Key is donor
        self.service_total = {} # Key is donor
        self.service_types = {} # Key is string; amounts in here are also in service_total, but broken down by service
        self.account_balances = {}
        self.assets = {}
        self.expense_total = {}

        self.statement_accounts = set()
        self.statement_assets = set()
        self.statement_expenses = set()
        self.statement_services = set()
        self.statement_mode = "normal"
        self.statement_current_month = None
        self.statement_monthly_add = decimal.Decimal(0)
        self.statement_monthly_subtract = decimal.Decimal(0)
        self.statement_monthly_closing_balance = decimal.Decimal(0)

        ## Totals for accounts

        # Cash flow sources, get more negative
        self.total_member_donations = decimal.Decimal(0)
        self.total_external_donations = decimal.Decimal(0)
        self.total_member_income = decimal.Decimal(0)
        self.total_external_income = decimal.Decimal(0)

        # Cash flow sinks, get more positive
        self.total_expenses = decimal.Decimal(0)

        # Loss in asset value due to depreciation, gets more positive
        self.total_depreciation = decimal.Decimal(0)

        # Asset gains, gets more negative
        self.total_member_asset_donations = decimal.Decimal(0)
        self.total_member_stock_donations = decimal.Decimal(0)
        self.total_external_asset_donations = decimal.Decimal(0)
        self.total_external_stock_donations = decimal.Decimal(0)

        # Loss in cash AND gain in value due to purchases - NON BALANCING QUANTITY, gets more positive
        self.asset_purchases = decimal.Decimal(0)
        self.stock_purchases = decimal.Decimal(0)

        # Loss in value of assets/stock due to sales, gets more positive
        self.asset_sales = decimal.Decimal(0)
        self.stock_sales = decimal.Decimal(0)

        # Gain in cash due to asset/stock sales, gets more negative
        self.asset_sale_income = decimal.Decimal(0)
        self.stock_sale_income = decimal.Decimal(0)

        # Known transaction IDs, used to deduplicate on bank import
        self.known_txnids = []

    # Statement mode - external interface

    def add_statement_account(self, acct):
        self.statement_accounts.add(acct)

    def add_statement_asset(self, asset):
        self.statement_assets.add(asset)

    def add_statement_expense(self, expense):
        self.statement_expenses.add(expense)

    def add_statement_service(self, service):
        self.statement_services.add(service)

    def set_statement_mode(self, mode):
        self.statement_mode = mode

    def _monthly_cashflow_dump(self):
        self.statement_monthly_closing_balance += self.statement_monthly_add + self.statement_monthly_subtract
        print ("%s,%s,%s,%s" % (self.statement_current_month, self.statement_monthly_add, self.statement_monthly_subtract, self.statement_monthly_closing_balance))
        self.statement_monthly_add = decimal.Decimal(0)
        self.statement_monthly_subtract = decimal.Decimal(0)

    def print_statement(self):
        self._monthly_cashflow_dump()

    # Statement mode - internals

    def statement(self, date, atype, account, amount, balance, reason):
        if self.statement_mode == "normal":
            print ("STATEMENT %s:%s: %s %s => %s %s" % (atype, account, date, amount, balance, reason))
        elif self.statement_mode == "monthly-cashflow":
            month = date.strftime("%Y-%m")
            if self.statement_current_month != None and self.statement_current_month != month:
                self._monthly_cashflow_dump()
            self.statement_current_month = month
            if amount > 0:
                self.statement_monthly_add += amount
            elif amount < 0:
                self.statement_monthly_subtract += amount

    def delta_account(self, date, account, amount, reason):
        if account not in self.account_balances:
            self.account_balances[account] = decimal.Decimal(0)

        self.account_balances[account] += amount

        if DEBUG or account in self.statement_accounts or "*" in self.statement_accounts:
            self.statement(date, "cash", account, amount, self.account_balances[account], reason)

    def delta_service_income(self, date, donor, service, amount):
        if donor not in self.service_total:
            self.service_total[donor] = decimal.Decimal(0)

        self.service_total[donor] += amount

        if service not in self.service_types:
            self.service_types[service] = decimal.Decimal(0)

        self.service_types[service] += amount

        if DEBUG or service in self.statement_services or "*" in self.statement_services:
            self.statement(date, "income", service, amount, self.service_types[service], donor)

    def delta_expense(self, date, name, amount):
        if name not in self.expense_total:
            self.expense_total[name] = decimal.Decimal(0)

        self.expense_total[name] += amount

        if DEBUG or name in self.statement_expenses or "*" in self.statement_expenses:
            self.statement(date, "expense", name, amount, self.expense_total[name], "")


    # FIXME: Add support for asset statements

    # Utilities

    def total_fixed_assets(self):
        total = decimal.Decimal(0)

        for asset in self.assets:
            if not self.assets[asset].isStock:
                total += self.assets[asset].value

        return total

    def total_stock(self):
        total = decimal.Decimal(0)

        for asset in self.assets:
            if self.assets[asset].isStock:
                total += self.assets[asset].value

        return total

    def total_cash(self):
        total = decimal.Decimal(0)
        for account in self.account_balances:
            if self.account_balances[account] > 0:
                total += self.account_balances[account]

        return total

    def total_debts(self):
        total = decimal.Decimal(0)
        for account in self.account_balances:
            if self.account_balances[account] < 0:
                total += self.account_balances[account]

        return total

    def compute_total_expenses(self):
        total = decimal.Decimal(0)
        for etype in self.expense_total:
            total += self.expense_total[etype]

        return total

    def compute_total_income(self):
        total1 = decimal.Decimal(0)
        for stype in self.service_types:
            total1 += self.service_types[stype]

        total2 = decimal.Decimal(0)
        for sdonor in self.service_total:
            total2 += self.service_total[sdonor]

        assert total1 == total2

        return total1

    def net_book_value(self):
        return self.total_cash() + self.total_debts() + self.total_stock() + self.total_fixed_assets()

    def net_income_and_expenditure(self):
        net_income = self.total_member_asset_donations + self.total_external_asset_donations
        net_income += self.total_member_stock_donations + self.total_external_stock_donations
        net_income += self.total_member_donations + self.total_external_donations
        net_income += self.total_member_income + self.total_external_income
        net_income += self.asset_sale_income + self.stock_sale_income # Income due to sales
        net_income += self.asset_sales + self.stock_sales # Loss in value due to sales
        net_income += self.total_expenses + self.total_depreciation

        return -net_income

    def check_invariants(self):
        if not DEBUG:
            return

        # Check fixed assets balance
        assert self.total_fixed_assets() == self.asset_purchases - self.total_member_asset_donations - self.total_external_asset_donations - self.asset_sales - self.total_depreciation

        # Check stock balances
        assert self.total_stock() == self.stock_purchases - self.total_member_stock_donations - self.total_external_stock_donations - self.stock_sales

        # Check cash balances
        assert self.total_cash() + self.total_debts() == -self.total_member_donations - self.total_external_donations - self.total_member_income - self.total_external_income - self.asset_purchases - self.stock_purchases - self.asset_sale_income - self.stock_sale_income - self.total_expenses

        # Check expenses add up
        assert self.compute_total_expenses() == self.total_expenses

        # Check incomes add up
        assert self.compute_total_income() == -(self.total_member_income + self.total_external_income)

        # Check balance sheet matches income and expenses
        assert self.net_book_value() == self.net_income_and_expenditure()

    def is_interesting(self, date):
        if self.maxdate:
            if date > self.maxdate:
                return False

        return True

    # Events

    def donation(self, date, donor, amount, account, txnid):
        self.known_txnids.append(txnid)

        if donor not in self.donation_total:
            self.donation_total[donor] = decimal.Decimal(0)

        self.donation_total[donor] += amount

        self.delta_account(date, account, amount, "Donation from %s" % donor)

        if type(donor) == int:
            self.total_member_donations -= amount
        else:
            self.total_external_donations -= amount
        self.check_invariants()

    def service(self, date, donor, service, amount, account, txnid):
        self.known_txnids.append(txnid)

        self.delta_service_income(date, donor, service, amount)

        self.delta_account(date, account, amount, "%s for %s" % (service, donor))

        if type(donor) == int:
            self.total_member_income -= amount
        else:
            self.total_external_income -= amount
        self.check_invariants()

    def transfer(self, date, from_acc, to_acc, amount, txnid):
        self.known_txnids.append(txnid)

        if from_acc not in self.account_balances:
            print ("WARNING: Transfer from unknown account %s" % from_acc)

        self.delta_account(date, from_acc, -amount, "Transfer to %s" % to_acc)
        self.delta_account(date, to_acc, +amount, "Transfer from %s" % from_acc)

        self.check_invariants()

    def asset_donation(self, donor, name, serial, descr, date, value, value_loss_per_year, txnid, isStock):
        self.known_txnids.append(txnid)

        if name in self.assets:
            # Addition to an existing asset
            a = self.assets[name]
            a.appreciate(value, value_loss_per_year, descr)
        else:
            a = Asset(name, serial, descr, date, value, value_loss_per_year, isStock)
            self.assets[name] = a

        if donor not in self.donation_total:
            self.donation_total[donor] = decimal.Decimal(0)

        self.donation_total[donor] += value

        if isStock:
            if type(donor) == int:
                self.total_member_stock_donations -= value
            else:
                self.total_external_stock_donations -= value
        else:
            if type(donor) == int:
                self.total_member_asset_donations -= value
            else:
                self.total_external_asset_donations -= value
        self.check_invariants()

    def asset_purchase(self, name, serial, descr, date, value, value_loss_per_year, account, txnid, isStock):
        self.known_txnids.append(txnid)

        if name in self.assets:
            # Addition to an existing asset
            a = self.assets[name]
            a.appreciate(value, value_loss_per_year, descr)
        else:
            a = Asset(name, serial, descr, date, value, value_loss_per_year, isStock)
            self.assets[name] = a

        self.delta_account(date, account, -value, "Purchase of %s" % name)

        if isStock:
            self.stock_purchases += value
        else:
            self.asset_purchases += value
        self.check_invariants()

    def asset_sale(self, name, descr, date, value, price, account, txnid, isStock):
        if price == None:
            price = value
        self.known_txnids.append(txnid)
        if name in self.assets:
            a = self.assets[name]
            a.depreciate(value, descr)
        else:
            print ("ERROR: Sale of unknown asset '%s'" % asset)
            sys.exit(1)

        self.delta_account(date, account, price, "Sale of %s" % name)

        if isStock:
            self.stock_sales += value
            self.stock_sale_income -= price
        else:
            self.asset_sales += value
            self.asset_sale_income -= price
        self.check_invariants()

    def expense(self, date, name, amount, account, txnid):
        self.known_txnids.append(txnid)

        self.delta_expense(date, name, amount)

        self.delta_account(date, account, -amount, "Expense %s" % name)

        self.total_expenses += amount
        self.check_invariants()

    def depreciation(self, date, asset, amount, txnid):
        self.known_txnids.append(txnid)

        a = self.assets[asset]

        if amount == None:
            amount = a.compute_depreciation()

        a.depreciate(amount)

        self.total_depreciation += amount
        self.check_invariants()

    # Reports

    def print_balance_sheet(self):
        if self.maxdate:
            print ("\n########## BALANCE SHEET AS AT %s ##########\n" % (self.maxdate,))
        else:
            print ("\n########## BALANCE SHEET AS AT TIME OF WRITING ##########\n")

        print ("ASSETS:\n")
        accounts_total = decimal.Decimal(0)
        accounts = list(self.account_balances.keys())
        accounts.sort()
        for account in accounts:
            if self.account_balances[account] != 0:
                print ("   %s: £%s" % (account, self.account_balances[account]))
            accounts_total += self.account_balances[account]

        print ("  SUBTOTAL (LIQUID ASSETS): £%s\n" % accounts_total)

        assets_total = decimal.Decimal(0)
        assets = list(self.assets.keys())
        assets.sort()
        for asset in assets:
            value = self.assets[asset].value
            if value != 0:
                print ("   %s: £%s" % (asset, value))
            assets_total += value
        print ("  SUBTOTAL (FIXED ASSETS): £%s" % (assets_total,))

        print ("\n  TOTAL: £%s\n" % (accounts_total + assets_total,))

    def print_pandl(self, old):
        mindate = tomorrow(old.maxdate)
        maxdate = self.maxdate

        print_centered("Income and Expenditure account for the period from %s to %s" % (mindate, maxdate))
        print

        print_cols("","",mindate,maxdate,"Change")
        print_cols("","Notes","£","£","£")

        old_trading_income = -(old.total_member_income + old.total_external_income + old.asset_sale_income + old.stock_sale_income)
        new_trading_income = -(self.total_member_income + self.total_external_income + self.asset_sale_income + self.stock_sale_income)

        print
        print_values("Trading Income","", old_trading_income, new_trading_income, new_trading_income - old_trading_income)

        old_member_donations = -(old.total_member_donations + old.total_member_asset_donations + old.total_member_stock_donations)
        old_external_donations = -(old.total_external_donations + old.total_external_asset_donations + old.total_external_stock_donations)

        new_member_donations = -(self.total_member_donations + self.total_member_asset_donations + self.total_member_stock_donations)
        new_external_donations = -(self.total_external_donations + self.total_external_asset_donations + self.total_external_stock_donations)

        # With the direct debit thingy, we can no longer actually distinguish these. FIXME: Stop calculating them as two different things...
        print_values("Donations","",old_member_donations+old_external_donations, new_member_donations+new_external_donations, (new_member_donations - old_member_donations) + (new_external_donations - old_external_donations))
#        print_values("Donations from members","",old_member_donations, new_member_donations, new_member_donations - old_member_donations)
 #       print_values("Donations from non-members","",old_external_donations, new_external_donations, new_external_donations - old_external_donations)
        old_turnover = old_trading_income + old_member_donations + old_external_donations
        new_turnover = new_trading_income + new_member_donations + new_external_donations

        print_cols("","","","--------")
        print_values("Turnover","",old_turnover, new_turnover, new_turnover - old_turnover)
        print
        old_cos = old.stock_sales + old.asset_sales
        new_cos = self.stock_sales + self.asset_sales
        print_values("Cost of sales","",old_cos, new_cos, new_cos - old_cos)
        old_gross_surplus = old_turnover - old.stock_sales - old.asset_sales
        new_gross_surplus = new_turnover - self.stock_sales - self.asset_sales
        print_values("Gross surplus","",old_gross_surplus, new_gross_surplus, new_gross_surplus-old_gross_surplus)
        print_values("Expenses","",old.total_expenses, self.total_expenses, self.total_expenses-old.total_expenses)
        print_values("Depreciation","",old.total_depreciation, self.total_depreciation, self.total_depreciation-old.total_depreciation)
        old_surplus = old_gross_surplus - old.total_expenses - old.total_depreciation
        new_surplus = new_gross_surplus - self.total_expenses - self.total_depreciation
        print_cols("","","","========")
        print_values("Operating surplus/defecit","",old_surplus, new_surplus, new_surplus-old_surplus)

        print
        print

        print ("Breakdown of Trading Income")
        for etype in self.service_types:
            new_total = self.service_types[etype]
            if etype in old.service_types:
                old_total = old.service_types[etype]
            else:
                old_total = decimal.Decimal("0")
            change = new_total - old_total

            print_values(etype,"",old_total, new_total, change)

        print_values("Asset sales","",-old.asset_sale_income, -self.asset_sale_income, -(self.asset_sale_income-old.asset_sale_income))
        print_values("Stock sales","",-old.stock_sale_income, -self.stock_sale_income, -(self.stock_sale_income-old.stock_sale_income))
        print_cols("","","--------","--------","--------")
        print_values("","",old_trading_income, new_trading_income, new_trading_income - old_trading_income)

        print
        print ("Breakdown of Expenses")
        for etype in self.expense_total:
            new_total = self.expense_total[etype]
            if etype in old.expense_total:
                old_total = old.expense_total[etype]
            else:
                old_total = decimal.Decimal("0")
            change = new_total - old_total
            print_values(etype,"",old_total, new_total, change)
        print_cols("","","--------","--------","--------")
        print_values("","",old.total_expenses, self.total_expenses, self.total_expenses - old.total_expenses)

    def print_abbreviated_accounts(self, old):
        mindate = tomorrow(old.maxdate)
        maxdate = self.maxdate

        header = "CHELTENHAM HACKSPACE LIMITED - REGISTERED NUMBER 9269647"

        print
        print
        print
        print_centered("Registered number 9269647")
        print
        print_centered("CHELTENHAM HACKSPACE LIMITED")
        print
        print_centered("Abbreviated Accounts")
        print
        print_centered("Year ending %s" % maxdate)

        print_newpage()
        print_centered(header)
        print
        print
        print_centered("Abbreviated Balance Sheet as at %s" % maxdate)
        print

        print_cols("","Notes",mindate,maxdate)
        print_cols("","","£","£")

        old_fixed_assets = old.total_fixed_assets()
        new_fixed_assets = self.total_fixed_assets()

        old_stock = old.total_stock()
        new_stock = self.total_stock()
        old_cash = old.total_cash()
        new_cash = self.total_cash()
        old_debts = old.total_debts()
        new_debts = self.total_debts()

        print
        print ("FIXED ASSETS")
        print_cols("Intangible assets","","-","-")
        print_values("Tangible assets","[3]",old_fixed_assets,new_fixed_assets)
        print_cols("Investments","","-","-")
        print_subtotal(old_fixed_assets,new_fixed_assets)

        print
        print ("CURRENT ASSETS")
        print_cols("Stocks","","-","-")
        print_cols("Debtors","","-","-")
        print_cols("Investments","","-","-")
        print_values("Stock","",old_stock,new_stock)
        print_values("Cash at bank and in hand","",old_cash,new_cash)
        old_current = old_stock+old_cash
        new_current = new_stock+new_cash
        print_subtotal(old_current,new_current)
        print
        print_cols("Prepayments and accrued income","","-","-")
        print_values("Creditors: due within one year","",old_debts,new_debts)
        print_values("Net current assets (liabilities)","",old_current, new_current)
        old_total = old_fixed_assets+old_current+old_debts
        new_total = new_fixed_assets+new_current+new_debts
        print_values("Total assets less current liabilities","",old_total,new_total)
        print_cols("Creditors: due after more than one year","","-","-")
        print_cols("Provisions for liabilities","","-","-")
        print_cols("Accruals and deferred income","","-","-")
        print_values("Total net assets (liabilities)","",old_total,new_total)

        print
        print ("RESERVES")
        print_cols("Revaluation reserve","","-","-")
        print_cols("Other reserves","","-","-")
        old_net_ie = old.net_income_and_expenditure()
        new_net_ie = self.net_income_and_expenditure()
        print_values("Income and expenditure account","",old_net_ie,new_net_ie)
        assert old_net_ie == old_total
        assert new_net_ie == new_total

        print
        print_values("Member's Funds","",old_total,new_total)

        print_newpage()
        print_centered(header)
        print
        print (" * For the year ending %s the company was entitled to exemption" % (maxdate,))
        print ("   under section 477 of the Companies Act relating to small companies.")
        print
        print (" * The members have not required the company to obtain an audit in")
        print ("   accordance with section 476 of the Companies Act 2006.")
        print
        print (" * The directors acknowledge their responsiblities for complying with")
        print ("   the requirements of the Act with respect to account periods and")
        print ("   the preparatino of accounts.")
        print
        print (" * These accounts have been prepared in accordance with the provisions")
        print ("   applicable to companies subject to the small companies regime.")
        print
        print ("Approved by the Board on _________")
        print ("And signed on their behalf by:")

        print_newpage()
        print_centered(header)
        print
        print_centered("Notes to the Abbreviated Accounts")
        print
        print (" [1] Accounting policies")
        print
        print ("The accounts have been prepared under the historical cost convention")
        print ("and in accordance with the Financial Reporting Standard for Smaller")
        print ("Entities effective April 2008.")
        print
        print (" [2] Company limited by guarantee")
        print
        print ("Company is limited by guarantee and consequently does not have share capital.")
        print
        print (" [3] Tangible fixed assets")
        print
        print_cols("","","£","")
        print ("COST")
        old_cost = old.asset_purchases - old.total_member_asset_donations - old.total_external_asset_donations - old.asset_sales
        print_value("At %s" % mindate,old_cost)
        additions = (self.asset_purchases - self.total_member_asset_donations - self.total_external_asset_donations) - (old.asset_purchases - old.total_member_asset_donations - old.total_external_asset_donations)
        print_value("Additions",additions)
        disposals = self.asset_sales - old.asset_sales
        print_value("Disposals",disposals)
        print_value("Revaluations",0)
        print_value("Transfers",0)
        new_cost = self.asset_purchases - self.total_member_asset_donations - self.total_external_asset_donations - self.asset_sales
        print_value("At %s" % maxdate, new_cost)
        assert new_cost == old_cost + additions - disposals
        print
        print ("DEPRECIATION")
        print_value("At %s" % mindate,old.total_depreciation)
        print_value("Charge for the year",self.total_depreciation - old.total_depreciation)
        print_value("At %s" % maxdate,self.total_depreciation)
        print
        print ("NET BOOK VALUES")
        print_value("At %s" % mindate,old_fixed_assets)
        print_value("At %s" % maxdate,new_fixed_assets)
        assert old_fixed_assets == old_cost - old.total_depreciation
        assert new_fixed_assets == new_cost - self.total_depreciation

    def import_statement(self, dry_run, bank_account):
        for line in sys.stdin:
            (txnid,date,kind,data,amount) = line.split("\t")
            if txnid in self.known_txnids:
                pass # Transaction already known
            else:
                amount = decimal.Decimal(amount)

                if dry_run:
                    df = open("%s.txt.tmp" % date, "a")
                else:
                    df = open("%s.txt" % date, "a")
                if kind == "Donation":
                    if data == "None" or data == "-":
                        df.write("external_donation donor:\"Anonymous\" amount:%s account:%s txnid:%s\n" % (amount, bank_account, txnid))
                    else:
                        df.write("member_donation member:%d amount:%s account:%s txnid:%s\n" % (int(data), amount, bank_account, txnid))
                elif kind == "Income":
                    (source, service) = data.split(":")
                    df.write("external_income from:%r service:%r amount:%s account:%s txnid:%s\n" % (source, service, amount, bank_account, txnid))
                elif kind == "Expense":
                    df.write("expense type:%s amount:%s account:%s txnid:%s\n" % (data, amount, bank_account, txnid))
                elif kind == "Unknown Income":
                    df.write("# FIXME: external_donation donor:%r amount:%s account:%s txnid:%s\n" % (data, amount, bank_account, txnid))
                    print ("FIXME: Unknown income in %s" % (date,))
                elif kind == "Unknown Expense":
                    df.write("# FIXME: expense type:%r amount:%s account:%s txnid:%s\n" % (data, amount, bank_account, txnid))
                    print ("FIXME: Unknown expense in %s" % (date,))
                elif kind == "Membership":
                    if data == "None" or data == "-":
                        df.write("member_service_fee member:999999 amount:%s service:'Membership' account:%s txnid:%s\n" % (amount, bank_account, txnid))
                    else:
                        df.write("member_service_fee member:%d amount:%s service:'Membership' account:%s txnid:%s\n" % (int(data), amount, bank_account, txnid))
                else:
                    print ("Error: Unknown statement line type %s" % (kind,))
                    sys.exit(1)
                df.close()

DATE_RE = re.compile(r"^([0-9][0-9][0-9][0-9])-([0-9][0-9])-([0-9][0-9])$")

def parse_date(date):
    m = DATE_RE.match(date)
    year = int(m.group(1))
    month = int(m.group(2))
    day = int(m.group(3))
    return datetime.date(year, month, day)

def yesterday(date):
    return date-datetime.timedelta(days=1)

def tomorrow(date):
    return date+datetime.timedelta(days=1)

def has_arg(args, field):
    return field in args

def get_arg(command, args, field, default=None):
    if field in args:
        return args[field]
    else:
        if default != None:
            return default
        else:
            print ("Error: Compulsory argument '%s' to %s missing in %r" % (field,command, args))
            sys.exit(1)

def get_int_arg(command, args, field, default=None):
    i = get_arg(command, args, field, default)
    try:
        return int(i)
    except:
        print ("Error: Invalid integer value '%s' given for argument %s of %s" % (i, field, command))
        sys.exit(1)

def get_money_arg(command, args, field, default=None):
    i = get_arg(command, args, field, default)
    try:
        return decimal.Decimal(i)
    except:
        print ("Error: Invalid money value '%s' given for argument %s of %s" % (i, field, command))
        sys.exit(1)

def process_command(acc, date, command, args):
    if not acc.is_interesting(date):
        return

    if command == "member_donation":
        member = get_int_arg(command, args, "member")
        amount = get_money_arg(command, args, "amount")
        access = get_money_arg(command, args, "access_fee", DEFAULT_MEMBER_ACCESS_FEE)
        account = get_arg(command, args, "account", DEFAULT_ACCOUNT)
        txnid = get_arg(command, args, "txnid", False)
        acc.donation(date, member, amount - access, account, txnid)
        acc.service(date, member, "Access to space for 1 month", access, account, txnid)
    elif command == "member_service_fee":
        member = get_int_arg(command, args, "member", None)
        user = get_arg(command, args, "user", member)
        amount = get_money_arg(command, args, "amount")
        account = get_arg(command, args, "account", DEFAULT_ACCOUNT)
        service = get_arg(command, args, "service")
        txnid = get_arg(command, args, "txnid", False)
        acc.service(date, user, service, amount, account, txnid)
    elif command == "transfer":
        from_account = get_arg(command, args, "from")
        to_account = get_arg(command, args, "to")
        amount = get_money_arg(command, args, "amount")
        txnid = get_arg(command, args, "txnid", False)
        acc.transfer(date, from_account, to_account, amount, txnid)
    elif command == "external_donation":
        donor = get_arg(command, args, "donor")
        amount = get_money_arg(command, args, "amount")
        account = get_arg(command, args, "account", DEFAULT_ACCOUNT)
        txnid = get_arg(command, args, "txnid", False)
        acc.donation(date, donor, amount, account, txnid)
    elif command == "external_income":
        source = get_arg(command, args, "from")
        service = get_arg(command, args, "service")
        amount = get_money_arg(command, args, "amount")
        account = get_arg(command, args, "account", DEFAULT_ACCOUNT)
        txnid = get_arg(command, args, "txnid", False)
        acc.service(date, source, service, amount, account, txnid)
    elif command == "member_asset_donation":
        member = get_int_arg(command, args, "member")
        value = get_money_arg(command, args, "value")
        name = get_arg(command, args, "name")
        serial = get_arg(command, args, "serial", "")
        descr = get_arg(command, args, "description")
        depreciation = get_money_arg(command, args, "depreciation", "0")
        txnid = get_arg(command, args, "txnid", False)
        acc.asset_donation(member, name, serial, descr, date, value, depreciation, txnid, False)
    elif command == "external_asset_donation":
        donor = get_arg(command, args, "donor")
        value = get_money_arg(command, args, "value")
        name = get_arg(command, args, "name")
        serial = get_arg(command, args, "serial", "")
        descr = get_arg(command, args, "description")
        depreciation = get_money_arg(command, args, "depreciation", "0")
        txnid = get_arg(command, args, "txnid", False)
        acc.asset_donation(donor, name, serial, descr, date, value, depreciation, txnid, False)
    elif command == "stock_purchase":
        account = get_arg(command, args, "account", DEFAULT_ACCOUNT)
        value = get_money_arg(command, args, "value")
        name = get_arg(command, args, "name")
        serial = get_arg(command, args, "serial", "")
        descr = get_arg(command, args, "description")
        depreciation = get_money_arg(command, args, "depreciation", "0")
        txnid = get_arg(command, args, "txnid", False)
        acc.asset_purchase(name, serial, descr, date, value, depreciation, account, txnid, True)
    elif command == "stock_sale":
        account = get_arg(command, args, "account", DEFAULT_ACCOUNT)
        value = get_money_arg(command, args, "value")
        name = get_arg(command, args, "name")
        descr = get_arg(command, args, "description")
        txnid = get_arg(command, args, "txnid", False)
        price = get_money_arg(command, args, "price", None)
        acc.asset_sale(name, descr, date, value, price, account, txnid, True)
    elif command == "asset_purchase":
        account = get_arg(command, args, "account", DEFAULT_ACCOUNT)
        value = get_money_arg(command, args, "value")
        name = get_arg(command, args, "name")
        serial = get_arg(command, args, "serial", "")
        descr = get_arg(command, args, "description")
        depreciation = get_money_arg(command, args, "depreciation", "0")
        txnid = get_arg(command, args, "txnid", False)
        acc.asset_purchase(name, serial, descr, date, value, depreciation, account, txnid, False)
    elif command == "asset_sale":
        account = get_arg(command, args, "account", DEFAULT_ACCOUNT)
        value = get_money_arg(command, args, "value")
        name = get_arg(command, args, "name")
        descr = get_arg(command, args, "description")
        txnid = get_arg(command, args, "txnid", False)
        price = get_money_arg(command, args, "price", None)
        acc.asset_sale(name, descr, date, value, price, account, txnid, False)
    elif command == "expense":
        name = get_arg(command, args, "type")
        amount = get_money_arg(command, args, "amount")
        account = get_arg(command, args, "account", DEFAULT_ACCOUNT)
        txnid = get_arg(command, args, "txnid", False)
        acc.expense(date, name, amount, account, txnid)
    elif command == "depreciation":
        asset = get_arg(command, args, "asset")
        if has_arg(args,"amount"):
            amount = get_money_arg(command, args, "amount")
        else:
            amount = None
        txnid = get_arg(command, args, "txnid", False)
        acc.depreciation(date, asset, amount, txnid)

LINE_CLEAN_RE = re.compile(r"^([^#]*)(?:#.*)?$")
COMMAND_RE = re.compile(r"^^([a-zA-Z0-9_.]+)[ \t]+(.*)$")
SHORT_ARG_RE = re.compile(r"^([a-zA-Z0-9_.]+)[ \t]*:[ \t]*([^ \t\"\']+)[ \t]*(.*)$")
LONG_ARG1_RE = re.compile(r"^([a-zA-Z0-9_.]+)[ \t]*:[ \t]*\"([^\"]+)\"[ \t]*(.*)$")
LONG_ARG2_RE = re.compile(r"^([a-zA-Z0-9_.]+)[ \t]*:[ \t]*\'([^\']+)\'[ \t]*(.*)$")

def parse_file(acc, date, filename):
    date = parse_date(date)
    if DEBUG:
        print ("%s" % (date,))
    for line in open(filename):
        m = LINE_CLEAN_RE.match(line)
        line = m.group(1)
        line = line.strip()
        if len(line) > 0:
            m = COMMAND_RE.match(line)
            if not m:
                print ("Invalid line: %s" % (line,))
                sys.exit(1)
            command = m.group(1)
            arghash = {}
            args = m.group(2)
            while len(args)>0:
                m = SHORT_ARG_RE.match(args)
                if not m:
                    m = LONG_ARG1_RE.match(args)
                    if not m:
                        m = LONG_ARG2_RE.match(args)

                if m:
                    arg_name = m.group(1)
                    arg_val = m.group(2)
                    arghash[arg_name] = arg_val
                    args = m.group(3)
                else:
                    print ("Invalid args: %s" % (args,))
                    sys.exit(1)
            process_command(acc, date, command, arghash)

# Set up

if len(sys.argv) == 1:
    print ("USAGE:")
    print
    print ("%s <date>" % (sys.argv[0],))
    print ("   ...to display balance sheet at date")
    print
    print ("%s <date> <date>" % (sys.argv[0],))
    print ("   ...to display profit/loss report over date range")
    print
    print ("%s --import-from-bank <account>" % (sys.argv[0],))
    print ("   ...to import transactions from a bank statement")
    print
    print ("%s --test-import-from-bank <account>" % (sys.argv[0],))
    print ("   ...to import transactions from a bank statement into [date].txt.tmp files")
    print
    print ("%s --abbreviated-accounts <date> <date>" % (sys.argv[0],))
    print ("   ...to display formal abbreviated accounts for the given financial period")
    print
    print ("Dates are YYYY-MM-DD, or 'NOW' for today")
    sys.exit(1)

def parse_date_or_now(d):
    if d == "NOW":
        return datetime.date.today()
    else:
        return parse_date(d)

old_accounts = None

if len(sys.argv) == 2:
    end = parse_date_or_now(sys.argv[1])
    accounts = Accounts(end)
    mode = "balance"

if len(sys.argv) == 3:
    if sys.argv[1] == "--import-from-bank" or sys.argv[1] == "--test-import-from-bank":
        bank = sys.argv[2]
        accounts = Accounts(False)
        mode = "bank-import"
        dry_run = sys.argv[1] == "--test-import-from-bank"
    else:
        start = parse_date_or_now(sys.argv[1])
        end = parse_date_or_now(sys.argv[2])
        old_accounts = Accounts(yesterday(start))
        accounts = Accounts(end)
        mode = "p&l"

if len(sys.argv) == 4:
    if sys.argv[1] == "--abbreviated-accounts":
        start = parse_date_or_now(sys.argv[2])
        end = parse_date_or_now(sys.argv[3])
        old_accounts = Accounts(yesterday(start))
        accounts = Accounts(end)
        mode = "abbreviated-accounts"
    elif sys.argv[1] == "--statement-cash":
        end = parse_date_or_now(sys.argv[2])
        account = sys.argv[3]
        accounts = Accounts(end)
        accounts.add_statement_account(account)
        mode = "statement"
    elif sys.argv[1] == "--monthly-cashflow":
        end = parse_date_or_now(sys.argv[2])
        account = sys.argv[3]
        accounts = Accounts(end)
        for acc in account.split(","):
            accounts.add_statement_account(acc)
        accounts.set_statement_mode("monthly-cashflow")
        mode = "statement"
    elif sys.argv[1] == "--statement-expense":
        end = parse_date_or_now(sys.argv[2])
        account = sys.argv[3]
        accounts = Accounts(end)
        accounts.add_statement_expense(account)
        mode = "statement"
    elif sys.argv[1] == "--statement-income":
        end = parse_date_or_now(sys.argv[2])
        account = sys.argv[3]
        accounts = Accounts(end)
        accounts.add_statement_service(account)
        mode = "statement"

# Read data

FN_DATE_RE = re.compile(r"^([0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]).txt$")

filenames = os.listdir(".")
filenames.sort()

for filename in filenames:
    m = FN_DATE_RE.match(filename)
    if m:
        date = m.group(1)
        parse_file(accounts, date, filename)
        if old_accounts:
            parse_file(old_accounts, date, filename)

# Output stuff

if mode == "balance":
    accounts.print_balance_sheet()

if mode == "p&l":
    accounts.print_pandl(old_accounts)

if mode == "bank-import":
    accounts.import_statement(dry_run, bank)

if mode == "abbreviated-accounts":
    accounts.print_abbreviated_accounts(old_accounts)

if mode == "statement":
    accounts.print_statement()
