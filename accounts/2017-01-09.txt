external_donation donor:"Petty cash" account:CASH amount:8
external_donation donor:"Arduino Course" amount:70 account:CASH
external_donation donor:"Loyalty Reward" amount:0.33 account:BANK txnid:3c5d09d02c299e2b1df95327a5006de002322243
member_donation member:79 amount:10.00 account:BANK txnid:9d7c7a1dc18f3430b3750f9f78abc5b8a98f9817
transfer from:CASH amount:50.00 to:BANK txnid:2d62de12900954a5223b43e40eb69606d96ed9da
expense type:BANK_CHARGES amount:6.50 account:BANK txnid:109d97f87b1dd9911560ba410f37bb6e473137f7
