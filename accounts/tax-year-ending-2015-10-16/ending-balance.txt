
########## BALANCE SHEET AS AT 2015-10-31 ##########

ASSETS:

   OWED: £0
   CASH/3DP: £0.00
   CASH: £57.32
   BANK: £925.00
  SUBTOTAL (LIQUID ASSETS): £982.32

   Prusa i3: £177.00
   Consumables: £50
   Hand tools: £50
   Arcade cabinet: £50
   Traffic lights: £20
   Test gear: £220
   Computers: £263
   Misc: £39.80
   PCB etching: £140
   Health and Safety: £5
   Column drill: £75
   Printer_B6200: £100.00
   Amenities: £105
   Projector_4100MP: £140.00
   Electric kiln: £100
   Furniture: £230
  SUBTOTAL (FIXED ASSETS): £1764.80

  TOTAL: £2747.12

