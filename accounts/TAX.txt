https://forum.hackspace.org.uk/t/hmrc-accounts-accountants-and-accounting-software/62/5

Forum post by Russ from London:

"This is my current summary of the corporation tax scenario, which I
wrote to someone in an email a few weeks back:

Assuming that (as with most spaces) members of your space are also
voting members of the company which runs it, unless you have a very
significant amount of trade with non-members, you should be paying no
corporation tax (CT, hereafter).

Let me break this down by income source:

    Membership subscriptions (which is the non-voluntary part of
    membership fee) are not considered a trade and any profit
    generated from these is not subject to CT.

    Donations (including any voluntary parts of a membership fee, and
    also including pledges which don't confer any special benefit on
    the person pledging) are never subject to CT.

    Sales to members (e.g. selling them drinks, etc) are also not
    trade and hence not subject to CT.

    Sales to non-members are the only part of a hackspace's income
    which should be subject to CT, but you can also offset any
    relevant expenses against the income when calculating the profit.

London Hackspace has never paid any corporation tax under these rules.

The relevant part of HMRC's Business Income Manual is here3.

If you have an accountant, point them to that URL and if they have any
further questions I am happy to answer them. If you don't, it's not
too heavy reading and contains many amusing references to
early-20th-century case law. Although, maybe I'm easily amused.

"Trade with members" is also frequently called "mutual trading" (which
is a similar but slightly different concept) and most accountants seem
to be more familiar with that term.

This is, of course, not professional advice. But I'm pretty confident
it's correct."
