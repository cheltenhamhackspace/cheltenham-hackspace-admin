external_donation donor:"Anonymous" amount:9.80 account:BANK txnid:613cdea907814a0707d4b86a4c700342d3b9edc7
external_donation donor:"Anonymous" amount:10.00 account:BANK txnid:aedd64954e0627743ed92c24b2513defb9f4fa20

# 3 year projected lifespan
depreciation asset:"Prusa i3" amount:88.50
depreciation asset:"Printer_B6200" amount:34.00
depreciation asset:"Projector_4100MP" amount:46
depreciation asset:"Misc" amount:13.27
# New this year: depreciation asset:"Black 3D printer" amount:56.67

# 5 years sounds reasonable for these things
depreciation asset:"Hand tools" amount:37.01
depreciation asset:"Textile tools" amount:29.80
depreciation asset:"Test gear" amount:44
depreciation asset:"PCB etching" amount:28
depreciation asset:"Column drill" amount:15
depreciation asset:"Soldering iron" amount:40
depreciation asset:"Electric kiln" amount:20
depreciation asset:"Power tools" amount:20.39
depreciation asset:"Furniture" amount:46
depreciation asset:"Amenities" amount:21

# Not really depreciating as we're not using them
# depreciation asset:"Arcade cabinet"
# depreciation asset:"Traffic lights"

# Not really depreciating as we turn over consumables (and they're mostly antiques that don't go out of date...)
# depreciation asset:"Consumables"

# Estimated valuation of our piles of components etc... £500? We were accounted for at £64.45 from purchase records, but we've had a lot of donations.
external_asset_donation donor:"UNKNOWN" value:435 name:Consumables description:"Various components and parts (estimated)"

